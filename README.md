# ECS 150 @ UC Davis

This repo contains the class materials created by Prof. Joël Porquet-Lupine for
the course *ECS 150: Operating Systems and System Programming*.

It contains all of the lecture material and some of the discussion material (the
material that was not specific to project assignments).

The videos were created during fall quarter 2020.

All of this work is shared under a [CC BY-NC-SA 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Copyright © 2017-2021 Joël Porquet-Lupine.
