---
layout: index
published: true
image: ./img/ecs150.png
---

# Welcome!

Hi all, my name is [Joël
Porquet-Lupine](https://cs.ucdavis.edu/directory/joel-porquet-lupine). I am
currently an Assistant Professor of Teaching at UC Davis.

ECS 150 is, by far, the course I have taught the most at UC Davis. I was even
hired, originally as a part-time lecture in 2017, in order to teach this course!

After giving this course more than 10 times over the years, I have probably
spent several hundreds of hours working on my slides, continuously improving
them quarter after quarter. And because of the pandemic, I also had to record
lecture videos in Fall of 2020.

This means that all the "lecturing" part of this course is now available
digitally, which I am happy to release entirely via this website under a [CC
BY-NC-SA 4.0 license](http://creativecommons.org/licenses/by-nc-sa/4.0/).

I hope that anyone who is interested in learning about operating systems will
find this course material useful!

# Course outline

## Lectures

ECS 150 is a 10-week course, but when accounting for holidays and/or exams,
there are about 9 weeks worth of lecture time.

The lecture material is broken up into 5 parts:

- OS Overview: ~2.5 weeks
    * [OS Introduction](./modules/os overview/os-introduction/)
    * [System Calls](./modules/os overview/syscalls/)
    * [OS Structure](./modules/os overview/os-structure/)
    * [The Kernel Abstraction](./modules/os overview/kernel-abstraction/)
- Process management: ~1.5 weeks
    * [Process Scheduling](./modules/process management/process-scheduling/)
    * [Concurrency and Threads](./modules/process management/concurrency-threads/)
- Synchronization: ~1.5 weeks
    * [Synchronization](./modules/synchronization/synchronization/)
    * [Deadlocks](./modules/synchronization/deadlock/)
- Storage: ~2 weeks
    * [Storage](./modules/storage/storage/)
    * [FS Abstraction](./modules/storage/fs-abstraction/)
    * [FS Implementation](./modules/storage/fs-implementation/)
- Virtual memory: ~1.5 weeks
    * [Address Translation](./modules/virtual memory/vm-translation/)
    * [Demand Paging](./modules/virtual memory/vm-paging/)

## Discussions

There is usually an hourly discussion per week. Although most discussions are
related to the various project assignments (which are not relevant to publish
here), some of them are tutorials or presentation related to operating systems
or systems programming.

- [Makefile tutorial](./modules/discussions/makefile/)
- [GDB tutorial](./modules/discussions/debugging/)
- [Internals of `printf()`](./modules/discussions/printf/)
- [Macros in C](./modules/discussions/c-macros/)
