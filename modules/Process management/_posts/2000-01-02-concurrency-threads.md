---
title: Concurrency and threads
---

## Slides

{% include slides.html id="concurrency-threads" %}

## Videos

### Part 1

{% include video.html id="1_1cvtvps7" %}

### Part 2

{% include video.html id="1_o5nxbw1j" %}

