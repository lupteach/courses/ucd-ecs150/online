---
title: Synchronization
---

## Slides

{% include slides.html id="sync" %}

## Videos

### Part 1

{% include video.html id="1_0e0n93k3" %}

### Part 2

{% include video.html id="1_y3f9lz82" %}

### Part 3

{% include video.html id="1_fb6fj85b" %}
