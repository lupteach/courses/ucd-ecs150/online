---
title: The Kernel Abstraction
---

## Slides

{% include slides.html id="kernel-abstraction" %}

## Videos

### Part 1

{% include video.html id="1_3tz9a2xc" %}

### Part 2

{% include video.html id="1_cce170cm" %}

### Part 3

{% include video.html id="1_y2sbweqd" %}
