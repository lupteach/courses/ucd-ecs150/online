---
title: FS Implementation
---

## Slides

{% include slides.html id="fs-implementation" %}

## Videos

### Part 1

{% include video.html id="1_kj838xty" %}

### Part 2

{% include video.html id="1_ohh0mk1v" %}

### Part 3

{% include video.html id="1_tsl14s5n" %}
